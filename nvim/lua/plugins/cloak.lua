return {
  "laytan/cloak.nvim",

  config = function()
    require("cloak").setup({
      enabled = true,
      cloak_character = "*",
      highlight_gropu = "Comment",
      patters = {
        {
          file_pattern = {
            ".env*",
            "wrangler.toml",
            ".dev.vars",
          },
          cloak_pattern = "=.+",
        },
      },
    })
  end,
}
